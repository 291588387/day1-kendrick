**ORID:**
**O:**
1. General procedure and time schedule of the whole agile full stack training in this month.
2. Learned from the "school in the cloud video and" that one's potential of active learning can be infinite. 
3. Introduction of "learning pyramid" and the concept of "PDCA" loop: plan, do, check and act.
4. Learned about concept map by specific example of bird in the afternoon, which is mainly made of concepts, relationships and propositions.
5. Drew the first concept map with my groups in class and held show case. 
6. Knew about the purpose of stand up meeting and "ORID" principle in summary and review of the day

**R:**

​	Astonished & a little bit upset

**I:**

1. From the case of the poor children I saw that one's potential of active learning can be infinite.
2. The silence during the practice is mainly due to lack of passionate and attractive communication atmosphere, which is quite unpleasant.

**D:**
1. It seems necessary for me to use more powerful tools like concept map in the future developing process.
2. It's an urge for me to improve my skills to conduct more effective and passionate communications.